using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public AudioSource[] sounds;


    public void PlaySound(SoundEffect sound)
    {
        int i = (int)sound;
        if (!sounds[i].isPlaying)
            sounds[i].Play();
    }
}    
public enum SoundEffect
{
    BottleCrash,
    MenCheer,
    BottleSmash
}
