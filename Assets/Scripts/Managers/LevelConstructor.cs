using System;
using System.Collections.Generic;
using UnityEngine;

public class LevelConstructor : MonoBehaviour
{
    public BP_Pickless[] models;
    public Level level;
    public int difficult;
    public Vector2 GridWidth = new Vector2(-9,9);
    public Vector2 GridHeight = new Vector2(-15, 15);
    public float scaleFactor;
    public void CreateNewLevel()
    {
        List<Vector3> grid = new List<Vector3>();
        List<Pickless> picklesses = new List<Pickless>();
        for (int i = 0; i < difficult; i++)
        {

            int type = UnityEngine.Random.Range(0, (int)PicklessType.MAX);
            Vector3 position;
            do
            {
                int x = (int) UnityEngine.Random.Range(GridWidth.x, GridWidth.y);
                int y = (int) UnityEngine.Random.Range(GridHeight.x, GridHeight.y);
                position = new Vector3(x, y, 0);
            } while (grid.Contains(position));
            grid.Add(position);

            Pickless pickless = new Pickless((PicklessType)type, position * scaleFactor);
            picklesses.Add(pickless);
        }

        level = new Level(level.name, picklesses.ToArray());
    }
    public void ReplenishLevel()
    {
        ClearScene();
        foreach (Pickless pickless in level.picklesses)
        {
            for (int i = 0; i < models.Length; i++)
            {
                if (models[i].type == pickless.type)
                {
                    GameObject obj = models[i].Instantiate(pickless.position, models[i].type != PicklessType.STATIC && models[i].type != PicklessType.RAND && UnityEngine.Random.Range(0,7) > 4);
                    obj.GetComponent<Collider>().enabled = true;
                    obj.name = models[i].prefab.name;
                    break;
                }
            }
        }
    }
    public void ClearScene()
    {
        MB_Pickless[] picklesses = GameObject.FindObjectsOfType<MB_Pickless>();
        foreach (MB_Pickless pickless in picklesses) {
            DestroyImmediate(pickless.gameObject);
        }
    }

    public void Clear()
    {
        level = new Level("", null);
    }
}
[Serializable]
public struct Level
{
    public string name;
    public Pickless[] picklesses;
    public Level(string name, Pickless[] picklesses)
    {
        this.name = name;
        this.picklesses = picklesses;
    }
}

[Serializable]
public struct Pickless
{
    public PicklessType type;
    public Vector3 position;

    public Pickless(PicklessType type, Vector3 position)
    {
        this.type = type;
        this.position = position;
    }
}
