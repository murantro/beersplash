using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager gm;
    public LevelConstructor lc;
    public UIManager ui;
    public SoundManager sm;
    //public LineRenderer lineRenderer;
    public UnityAction cheer;
    public UnityAction GameEnd;

    public float timer;
    public GameStatus status;

    public float left;
    public float right;
    public float top;
    public float bottom;

    public float Top
    {
        get
        {
            return top - ScaleFactor;
        }
    }
    public float Bottom
    {
        get
        {
            return bottom + ScaleFactor;
        }
    }
    public float Right
    {
        get
        {
            return right - ScaleFactor;
        }
    }

    public float Left
    {
        get
        {
            return left + ScaleFactor;
        }
    }
    public int bestRecord;
    public int points;
    public int movements;
    public float ScaleFactor
    {
        get
        {
            return 0.5f * lc.scaleFactor;
        }
    }
    // Start is called before the first frame update
    void Awake()
    {
        if (gm == null)
        {
            gm = this;
        }

        left = (GameManager.gm.lc.GridWidth.x - 0.5f) * lc.scaleFactor;
        right = (GameManager.gm.lc.GridWidth.y + 0.5f) * lc.scaleFactor;
        top = (GameManager.gm.lc.GridHeight.y + 0.5f) * lc.scaleFactor;
        bottom = (GameManager.gm.lc.GridHeight.x - 0.5f) * lc.scaleFactor;
        //Invoke("StartGame", 0.5f);
        bestRecord = PlayerPrefs.GetInt("BestPoints");
    }
    /*private void Start()
    {
        lineRenderer.SetPosition(0, new Vector3(left, top, 0.0f));
        lineRenderer.SetPosition(1, new Vector3(right, top, 0.0f));
        lineRenderer.SetPosition(2, new Vector3(right, bottom, 0.0f));
        lineRenderer.SetPosition(3, new Vector3(left, bottom, 0.0f));
        lineRenderer.SetPosition(4, new Vector3(left, top, 0.0f));
    }*/
    public void StartGame()
    {
        lc.CreateNewLevel();
        lc.ReplenishLevel();
        StartCoroutine(Timer());
    }

    private IEnumerator Timer()
    {
        timer = 60;
        points = 0;
        movements = 0;
        ui.RefreshBest(bestRecord);
        status = GameStatus.Start;
        do
        {
            timer -= Time.deltaTime;
            yield return null;
            ui.RefreshTimer();
        } while (timer > 0);
        status = GameStatus.End;
        ui.EndGame();
        if (bestRecord < points)
        {
            bestRecord = points;
            PlayerPrefs.SetInt("BestPoints", bestRecord);
            ui.RefreshBest(bestRecord);
        }
        GameEnd?.Invoke();
    }

    public void PickUp(int reward)
    {
        sm.PlaySound(SoundEffect.MenCheer);
        points += reward;
        if (reward > 0) {
            movements++;
            timer += 1;
        }
        else
        {
            Debug.Log("Bad point");
            timer -= 10;
        }
        ui.RefreshStatus();
        cheer?.Invoke();
    }
    public void ExitGame()
    {
        Application.Quit();
    }
    
}

public enum GameStatus
{
    Start,
    End
}
