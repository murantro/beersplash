using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text pointsText;
    public Text finalPointsText;
    public Text recordPointsText;
    public Text lifeText;
    public Text timerText;
    public Text startGameText;
    public Text titleGameText;
    public GameObject MainPanel;
    public GameObject FacebookShareButton;

    public void RefreshStatus()
    {
        Debug.Log("Refreshing status...");
        pointsText.text = "Points: " + GameManager.gm.points;
        lifeText.text = "Life: " + GameManager.gm.movements;
    }

    public void RefreshTimer()
    {
        timerText.text = ((int)GameManager.gm.timer).ToString();
    }
    public void RefreshBest(int bestRecords)
    {

        recordPointsText.text = "Best: " + bestRecords;
    }
    public void EndGame()
    {
        finalPointsText.text = "Points: " + GameManager.gm.points;
        startGameText.text = "Retry";
        titleGameText.text = "Game Over";

        MainPanel.SetActive(true);
        FacebookShareButton.SetActive(true);
    }
}
