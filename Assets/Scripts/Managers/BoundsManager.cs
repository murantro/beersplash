using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoundsManager : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        MB_Pickless pickless = other.GetComponent<MB_Pickless>();
        Vector3 position = pickless.targetPosition;
        Vector2 offsetPostion = other.transform.position;
        Vector3 newPostion = position;
        if (position.x > GameManager.gm.right)
        {
            offsetPostion = new Vector2(-position.x, offsetPostion.y);
            newPostion = new Vector3(GameManager.gm.Left, newPostion.y, newPostion.z);
        }
        if (position.x < GameManager.gm.left)
        {
            offsetPostion = new Vector2(-position.x, offsetPostion.y);
            newPostion = new Vector3(GameManager.gm.Right, newPostion.y, newPostion.z);
        }
        if (position.y > GameManager.gm.top)
        {
            offsetPostion = new Vector2(offsetPostion.x, -position.y);
            newPostion = new Vector3(newPostion.x, GameManager.gm.Bottom, newPostion.z);
        }
        if (position.y < GameManager.gm.bottom)
        {
            offsetPostion = new Vector2(offsetPostion.x, -position.y);
            newPostion = new Vector3(newPostion.x, GameManager.gm.Top, newPostion.z);
        }
        GameObject obj = pickless.model.Instantiate(offsetPostion, pickless.model.type == PicklessType.ZERO);
        obj.name = pickless.model.prefab.name;
        obj.GetComponent<MB_Pickless>().NewPosition(newPostion);
        Destroy(other.gameObject);

    }
}
