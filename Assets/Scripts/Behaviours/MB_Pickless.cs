using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MB_Pickless : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer spriteRenderer;
    Transform _transform;
    public BP_Pickless model;
    public bool isEmpty;
    public bool isMoving;
    private void Awake()
    {
        _transform = transform;
    }
    public void OnMouseDown()
    {
        if (isEmpty || GameManager.gm.status == GameStatus.End) return;
        if (model.type == PicklessType.DESTROY)
        {
            GameManager.gm.PickUp(model.points);
        }
        GameManager.gm.movements--;
        
        model.OnPick(this);
    }
    public Vector3 targetPosition;
    IEnumerator Move(Vector3 position)
    {
        //targetPosition = position;
        isMoving = true;
        Vector3 distance;
        float sqrLen;
        Vector2 offsetPostion = _transform.position;
        Vector3 newPostion = position;
        bool isOutSide = false;
        if (position.x > GameManager.gm.right)
        {
            offsetPostion = new Vector2(-position.x, offsetPostion.y);
            newPostion = new Vector3(GameManager.gm.Left, newPostion.y, newPostion.z);

            isOutSide = true;
        }
        if (position.x < GameManager.gm.left)
        {
            offsetPostion = new Vector2(-position.x, offsetPostion.y);
            newPostion = new Vector3(GameManager.gm.Right, newPostion.y, newPostion.z);
            isOutSide = true;
        }
        if (position.y > GameManager.gm.top)
        {
            offsetPostion = new Vector2(offsetPostion.x, -position.y);
            newPostion = new Vector3(newPostion.x, GameManager.gm.Bottom, newPostion.z); 
            isOutSide = true;
        }
        if (position.y < GameManager.gm.bottom)
        {
            offsetPostion = new Vector2(offsetPostion.x, -position.y);
            newPostion = new Vector3(newPostion.x, GameManager.gm.Top, newPostion.z);
            isOutSide = true;
        }
        if (isOutSide)
        {
            GameObject obj = model.Instantiate(offsetPostion, isEmpty);
            obj.name = model.prefab.name;
            obj.GetComponent<MB_Pickless>().NewPosition(newPostion);
        }
        do
        {
            _transform.position = Vector2.Lerp(_transform.position, position, Time.deltaTime * model.speed);
            distance = _transform.position - position;
            sqrLen = distance.sqrMagnitude;
            yield return null;
        } while (sqrLen > 0.01);
        if (isOutSide)
        {
            Destroy(gameObject);
        }
        transform.position = position;
        GetComponent<Collider>().enabled = true;
        isMoving = false;
    }
    public void NewPosition(Vector3 position)
    {
        StartCoroutine(Move(position));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Pickless")) return;
        //empty never gonna have moving to true
        if (!isMoving)
        {
            model.EmitParticles(transform.position);
            if (model.type != PicklessType.DESTROY)
            {
                GameManager.gm.sm.PlaySound(SoundEffect.BottleCrash);
            }
            else
            {
                GameManager.gm.sm.PlaySound(SoundEffect.BottleSmash);
            }
            if (isEmpty)
            {
                isEmpty = false;
                spriteRenderer.sprite = model.sprite;
                return;
            }
            GameManager.gm.PickUp(model.points);
        }
        Destroy(gameObject);
    }
    
    public void SetModel(BP_Pickless model, bool isEmpty)
    {
        this.isEmpty = isEmpty;
        this.model = model;
        spriteRenderer.sprite = isEmpty?model.empty:model.sprite;
    }
}
