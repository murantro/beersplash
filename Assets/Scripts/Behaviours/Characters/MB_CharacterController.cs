using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MB_CharacterController : MonoBehaviour
{
    [SerializeField]
    Animator animator;
    [SerializeField]
    GameObject beer;

    private float thresholdTime;
    private float currentTime;
    private int index=0;
    private CharacterStatus status;
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        FireTalking(Random.Range(1,8));
        GameManager.gm.cheer += Cheer;
    }
    private void OnDestroy()
    {
        GameManager.gm.cheer -= Cheer;
    }

    void Update()
    {
        currentTime += Time.deltaTime;
        if (currentTime > thresholdTime)
        {
            SetThresholdTime();
            if (index != 0)
            {
                FireCheer(index, false);
                index = 0;
            }
            else if (status != CharacterStatus.Drinking) 
            { 
                if (Random.Range(0,3) > 1)
                {
                    FireDrink();
                }
                else
                {
                    FireTalking(Random.Range(1, 8));
                }
            }
        }
    }
    private void Cheer()
    {
        if (index == 0)
        {
            SetThresholdTime();
            index = Random.Range(1, 8);
            FireCheer(index, true);
        }
    }
    private void SetThresholdTime()
    {
        thresholdTime = Random.Range(0.5f,3.0f);
        currentTime = 0;
    }
    public void FireDrink()
    {
        status = CharacterStatus.Drinking;
        animator.SetTrigger("Drink");
    }
    public void ActivateBeer()
    {
        beer.SetActive(true);
    }
    public void DisableBeer()
    {
        beer.SetActive(false);
        status = CharacterStatus.Idle;
    }

    public void FireTalking(int i)
    {
        if (status == CharacterStatus.Drinking)
            DisableBeer();
        status = CharacterStatus.Talking;
        animator.SetTrigger("Talking" + i);
    }
    public void FireCheer(int i, bool enabled)
    {
        if (status == CharacterStatus.Drinking)
            DisableBeer();
        status = CharacterStatus.Cheer;
        animator.SetBool("Cheer" + i, enabled);
    }
}

enum CharacterStatus
{
    Idle,
    Talking,
    Cheer,
    Drinking,
    MAX
}
