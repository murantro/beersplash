using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pickless_D", menuName = "Pickless/Destroy")]
public class BP_PicklessDestroy : BP_Pickless
{
    [SerializeField]
    float RadiusDestruction;
    public override void OnPick(MB_Pickless pickless)
    {
        RaycastHit[] hits = Physics.SphereCastAll(pickless.transform.position, RadiusDestruction, pickless.transform.forward, 0, LayerMask.GetMask("Beer"));
        if (hits.Length > 0)
        {
            GameManager.gm.sm.PlaySound(SoundEffect.BottleSmash);
            foreach (RaycastHit hit in hits)
            {
                EmitParticles(hit.collider.transform.position);
                Destroy(hit.collider.gameObject);
            }
        }
    }
}
