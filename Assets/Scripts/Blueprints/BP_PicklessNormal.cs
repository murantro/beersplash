using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pickless_N", menuName = "Pickless/Normal")]
public class BP_PicklessNormal : BP_Pickless
{
    public override void OnPick(MB_Pickless pickless)
    {
        Vector3 current = pickless.transform.position;
        for (int i=0; i < positions.Length; i++)
        {
            Vector3 newPosition = current + positions[i];
            MB_Pickless newPickless = Instantiate(current, false).GetComponent<MB_Pickless>();
            newPickless.NewPosition(newPosition);
        }
        if (type != PicklessType.STATIC)
            Destroy(pickless.gameObject);
    }
}
