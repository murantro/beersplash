using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BP_Pickless : ScriptableObject
{
    [SerializeField]
    protected BP_Pickless[] models;
    [SerializeField]
    private GameObject pickupPaticlesPrefab;
    public Sprite sprite;
    public Sprite empty;
    public PicklessType type;
    public GameObject prefab;
    public float speed;
    public int points;
    public Vector3[] positions;
    public abstract void OnPick(MB_Pickless pickless);

    public virtual GameObject Instantiate(Vector3 position, bool isEmpty)
    {
        GameObject obj = Instantiate(prefab, position, Quaternion.identity);
        obj.name = "Pickless_" + type;
        if (models != null && models.Length > 0 && Random.Range(0, 3) > 1)
        {
            int i = Random.Range(0, models.Length);
            obj.GetComponent<MB_Pickless>().SetModel(models[i], models[i].type == PicklessType.ZERO);
        }
        else
            obj.GetComponent<MB_Pickless>().SetModel(this, isEmpty);
        return obj;
    }
    public void EmitParticles(Vector3 position)
    {
        Instantiate(pickupPaticlesPrefab, position, Quaternion.identity);
    }
}

public enum PicklessType
{
    SC,
    SS,
    LR,
    TB,
    TRBL,
    TLBR,
    ALL,
    RAND,
    ZERO,
    DESTROY,
    STATIC,
    MAX
}