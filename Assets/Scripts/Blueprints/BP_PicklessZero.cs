using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pickless_Z", menuName = "Pickless/Zero")]
public class BP_PicklessZero : BP_Pickless
{
    public override void OnPick(MB_Pickless pickless)
    {
    }

    public override GameObject Instantiate(Vector3 position, bool isEmpty)
    {
        int selected = Random.Range(0, models.Length);
        return models[selected].Instantiate(position, true);
    }
}
