using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FacebookConfig", menuName = "FacebookConfig")]
public class BP_Facebook : ScriptableObject
{
    public string ShareLink;
    public string image;
    public string title;
    public string description;
}
