using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pickless_S", menuName = "Pickless/Star")]
public class BP_PicklessAll : BP_Pickless
{
    public override void OnPick(MB_Pickless pickless)
    {
        int amoung = Random.Range(0, models.Length);
        List<Vector3> selectedPositions = new List<Vector3>();
        do
        {
            int i = Random.Range(0, positions.Length);
            if (!selectedPositions.Contains(positions[i]))
            {
                selectedPositions.Add(positions[i]);
            }
        } while (selectedPositions.Count < amoung);
        Vector3 current = pickless.transform.position;
        for (int i = 0; i < selectedPositions.Count; i++)
        {
            Vector3 newPosition = current + selectedPositions[i];
            MB_Pickless newPickless = models[i].Instantiate(current, models[i].type == PicklessType.ZERO).GetComponent<MB_Pickless>();
            newPickless.NewPosition(newPosition);
        }
        Destroy(pickless.gameObject);
    }
}
