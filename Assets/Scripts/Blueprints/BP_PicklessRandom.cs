using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Pickless_R", menuName = "Pickless/Random")]
public class BP_PicklessRandom : BP_Pickless
{
    public override void OnPick(MB_Pickless pickless)
    {
        int i = Random.Range(0, positions.Length);
        Vector3 current = pickless.transform.position;

        Vector3 newPosition = current + positions[i];
        MB_Pickless newPickless = Instantiate(current, false).GetComponent<MB_Pickless>();
        newPickless.NewPosition(newPosition);

        Destroy(pickless.gameObject);
    }
}

