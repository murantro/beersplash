using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.IO;

[CustomEditor(typeof(LevelConstructor))]
public class LevelConstructorEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        LevelConstructor levelBuilder = (LevelConstructor)target;
        if (GUILayout.Button("Create Level"))
        {
            levelBuilder.CreateNewLevel();
        }
        if (!string.IsNullOrEmpty(levelBuilder.level.name))
        {
            if (GUILayout.Button("Replenish Level"))
            {
                levelBuilder.ReplenishLevel();
            }

            if (GUILayout.Button("Load Level"))
            {
                LoadLevel(levelBuilder.level);
            }

            if (GUILayout.Button("Save Level"))
            {
                SaveLevel(levelBuilder.level);
            }

            if (GUILayout.Button("Clear Level"))
            {
                levelBuilder.Clear();
            }
        }
    }
    public void SaveLevel(Level level)
    {
        string path = Application.dataPath + "/Resources/Levels/" + level.name + ".json";
        string str = JsonUtility.ToJson(level);
        FileStream fs = new FileStream(path, FileMode.Create);
        StreamWriter writer = new StreamWriter(fs);
        writer.Write(str);
        AssetDatabase.Refresh();
    }
    public Level LoadLevel(Level level)
    {
        string filePath = "Levels/" + level.name;
        TextAsset targetFile = Resources.Load<TextAsset>(filePath);
        return JsonUtility.FromJson<Level>(targetFile.text);

    }
}
